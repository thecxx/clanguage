#include <stdio.h>
#include <stdlib.h>

int main(int argc, char **argv)
{

    int i, j, table[9][9] = {0};

    // 存入乘法表
    for(i = 0; i < 9; i++) {
        for(j = 0; j <= i; j++) {
            table[i][j] = (i+1) * (j+1);
        }
    }

    // 输出
    for(i = 0; i < 9; i++) {
        for(j = 0; j <= i; j++) {
            printf("%5d", table[i][j]);
        }
        printf("\n");
    }

    /* 防止调试器不自动中断 */
    getchar();

    return 0;
}
