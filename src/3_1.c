#include <stdio.h>
#include <stdlib.h>

int main(int argc, char **argv)
{

    int i = 1, j;

    for(; i <= 9; i++) {
        // 输出特定数量空格符
        for(j = 1; j <= 9 - i; j++)
            printf("    ");
        // 输出左边
        for(j = 1; j < i; j++)
            printf("%-4d", j);
        // 输出右边
        for(; j >= 1; j--)
            printf("%-4d", j);
        printf("\n");
    }

    /* 防止调试器不自动中断 */
    getchar();

    return 0;
}
