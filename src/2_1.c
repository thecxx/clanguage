#include <stdio.h>
#include <stdlib.h>

int main(int argc, char **argv)
{
    int x, y, z;

    do {
        // 提示语
        printf("输入两个非零自然数(空格分割):");
        // 接收数值
        scanf("%d%d", &x, &y);
    } while(x <= 0 || y <= 0);

    // 保证x中存放较大数
    if (x < y) {
        // 交换数值
        x ^= y;
        y ^= x;
        x ^= y;
    }

    // x与y求积数
    z = x * y;

    // 求最大公约数
    while(x % y != 0) {
        x %= y;
        // 交换数值
        x ^= y;
        y ^= x;
        x ^= y;
    }

    printf("最大公约数: %d 最小公倍数: %d\n", y, z / y);

    /* 防止调试器不自动中断 */
    getchar();

    return 0;
}
