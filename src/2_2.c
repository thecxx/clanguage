#include <stdio.h>
#include <stdlib.h>

int main(int argc, char **argv)
{
    int x, y, z;

    /**
     * 设： 大马x 中马y 小马z
     *
     * 则
     *      x  + y  + z      = 100
     *      3x + 2y + (1/2)z = 200
     *
     * 所以
     *      5x + 3y = 300
     *      x <= 66
     *      y <= 100
     *
     */

    for(x = 1; x <= 66; x++) {
        for(y = 1; y <= 100; y++) {
            if(5 * x + 3 * y == 300) {
                printf("可选方案: 大马%2d匹, 中马%2d匹, 小马%2d匹\n", x, y, 100 - x - y);
            }
        }
    }

    /* 防止调试器不自动中断 */
    getchar();

    return 0;
}
