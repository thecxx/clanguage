#include <stdio.h>
#include <stdlib.h>

int main(int argc, char **argv)
{

    float score = 0.0;
    char ch = 'E';

    printf("请输入学生分数:");
    scanf("%f", &score);

    switch(((int)score)/10)
    {
        case 9:
        case 10:
            ch = 'A';
            break;
        case 8:
            ch = 'B';
            break;
        case 7:
            ch = 'C';
            break;
        case 6:
            ch = 'D';
            break;
        default:
            ch = 'E';
    }

    // 输出评分结果
    printf("该同学得分:%.2f,评分:%c\n", score, ch);

    /* 防止调试器不自动中断 */
    getchar();

    return 0;

}
