#include <stdio.h>
#include <stdlib.h>

int main(int argc, char **argv)
{
    // 初始化数组
    int i, j, x[10][10] = {0};

    // 生成数据
    for(i = 0; i < 10; i++) {
        for(j = 0; j <= i; j++) {
            x[i][j] = j + 1;
        }
    }

    // 输出数组
    for(i = 0; i < 10; i++) {
        for(j = 0; j < 10; j++) {
            printf("%4d", x[i][j]);
        }
        printf("\n");
    }

    /* 防止调试器不自动中断 */
    getchar();

    return 0;
}
