#include <stdio.h>
#include <stdlib.h>

int main(int argc, char **argv)
{

    double f = 0.0, c=0.0;

    printf("请输入华氏摄氏度:");
    scanf("%lf", &f);

    c = 5.0/9*(f-32);

    printf("华氏度:%.2f,摄氏度:%.2f\n", f, c);

    /* 防止调试器不自动中断 */
    getchar();

    return 0;
}
